package com.zhandev;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CiCdDemoTest {
	
	CiCdDemo ciCdDemo = new CiCdDemo();
	
	@Test
	public void testddInt() {
		assertEquals(ciCdDemo.addInt(1, 2), 3);
		assertEquals(ciCdDemo.addInt(1, 3), 4);
	}
	
	
}
